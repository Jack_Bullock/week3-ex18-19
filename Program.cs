﻿using System;

namespace week3_ex18_19
{
    class Program
    {
        static void Main(string[] args)
        {
        Console.Clear();
        Console.WriteLine("EX18 TASK A");
        var num1= 5;
        var num2= 1;
        var num3= 16;
        var num4= 15;
        var num5= 9;
        Console.WriteLine($"the value of num1= {num1}");
        Console.WriteLine($"the value of num2= {num2}");
        Console.WriteLine($"the value of num3= {num3}");
        Console.WriteLine($"the value of num4= {num4}");
        Console.WriteLine($"the value of num5= {num5}");
        Console.WriteLine("********************************************************");
        Console.WriteLine("EX18 Task B");
        var num6= 10;
        Console.WriteLine($"num1 + num2= {num1+num2}");
        Console.WriteLine($"num3 + num4= {num3+num4}");
        Console.WriteLine($"num5 + num6= {num5+num6}");
        Console.WriteLine("********************************************************");
        Console.WriteLine("EX18 Task C");
        Console.WriteLine($"First 4 varibles add together={num1+num2+num3+num4}");
        Console.WriteLine("********************************************************");
        Console.WriteLine("EX18 Task D");
        Console.WriteLine("(I'm Thinking...)*3");
        
        Console.WriteLine($"Question K | (6\xB2 + 7) * 3 + 2 = {(Math.Pow(6,2) + 7) * 3 + 2 }"); 
        Console.WriteLine($"Question A | (6 + 7) * (3 + 2) = {((6 + 7) * 3 - 2 )}");

   
        
        
        //End the program with blank line and instructions
        Console.ResetColor();
        Console.WriteLine();
        Console.WriteLine("Press <Enter> to quit the program");
        Console.ReadKey();
        }
    }
}
